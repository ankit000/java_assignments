package com.bean;

import org.springframework.jdbc.core.JdbcTemplate;

public class Dao {
	private JdbcTemplate template;
	public void setTemplate (JdbcTemplate template)
	{
		this.template = template;
	}
	public int insert(SignupForm form)
	{
		String qry = SignupformQuery.savedata;
		return template.update(qry,form.getEmail(),form.getNumber(),form.getName(),form.getUsername(),form.getPassword());
	}
}
