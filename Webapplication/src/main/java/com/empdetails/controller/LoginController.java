package com.empdetails.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bean.Dao;
import com.bean.SignupForm;
import com.bean.SignupformQuery;

@Controller
public class LoginController {
	@Autowired
	Dao dao;

	public Dao getDao()
	{
		return dao;
	}

	public void setDao(Dao dao)
	{
		this.dao = dao;
	}


	@RequestMapping ("/login")
	public String loginPage (@ModelAttribute ("form") SignupForm form,@RequestParam ("username") String username,@RequestParam ("password") String password,Model model)
	{
		if(username == username && password == password)
		{
			return "userlogin";
		}
		else
		{
			return "errorlogin";
		}
	}

	@RequestMapping ("/forgot")
	public String forgotPass()
	{
		return "forgotpass";
	}

	@RequestMapping ("/signup")
	public String signUp()
	{
		return "signupform";
	}

	@RequestMapping ("/signupsuccess")
	public String signupSuccess(@ModelAttribute ("form") SignupForm form)
	{
		dao.insert(form);
		return "signupsuccess";
	}

	@RequestMapping ("/home")
	public String homePage()
	{
		return "home";
	}
}